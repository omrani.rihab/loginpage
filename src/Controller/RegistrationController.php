<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Utilisateur;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UtilisateurType;
use Doctrine\Persistence\ManagerRegistry;

use function PHPUnit\Framework\isNull;

class RegistrationController extends AbstractController
{
    #[Route('/registration', name: 'app_registration')]
    public function index(): Response
    {
        return $this->render('registration/index.html.twig', [
            'controller_name' => 'RegistrationController',
        ]);
    }

    /**
 * @Route("/new", name="utilisateur_new", methods={"GET","POST"})
 */
public function new(Request $request, UserPasswordHasherInterface $passwordHasher, ManagerRegistry $doctrine): Response
{
        $utilisateur = new Utilisateur();
        $form = $this->createForm(UtilisateurType::class, $utilisateur);
        $form->handleRequest($request);
        $entityManager = $doctrine->getManager();

        $repository = $entityManager->getRepository(Utilisateur::class);

        // find user exist
      
        $user = $repository->findOneBy([
            'username' => $form["username"]->getData(),
           
        ]);

       if ($form->isSubmitted() && $form->isValid() && is_null($user)) {
                //encodage du mot de passe
                $utilisateur->setPassword(
                $passwordHasher->hashPassword($utilisateur, $utilisateur->getPassword()));
                $entityManager->persist($utilisateur);
                $entityManager->flush();

               // return $this->redirectToRoute('utilisateur_show', $utilisateur->getId());

                return $this->redirectToRoute('utilisateur_show', array(
                    'id' => $utilisateur->getId()
                ));
        }

        return $this->render('utilisateur/new.html.twig', [
        'utilisateur' => $utilisateur,
        'form' => $form->createView(),
        ]);
}

 /**
         * @Route("/{id}", name="utilisateur_show", methods={"GET"})
         */
        public function show(Utilisateur $utilisateur): Response
        {
                //accès géré dans le security.yaml
                return $this->render('utilisateur/show.html.twig', [
                'utilisateur' => $utilisateur,
                ]);
        }

}
